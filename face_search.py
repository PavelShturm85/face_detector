import cv2
import datetime
import math
import face_recognition
import json
import redis
import uuid
from pymongo import MongoClient

settings = dict(
    mongoIP="127.0.0.1",
    mongoPort=27017,
    typeCam=0,
    delta_time=5.0,
    num_frame_for_check=5,
    step_frame_for_check=10,
    redisIP="localhost",
    redisPort=6379,
    redisDB=0,
    image_scale_factor=4
)


class PersonRecognition():
    def __init__(self, *args, **kwargs):
        mongo = MongoClient(settings["mongoIP"],
                            settings["mongoPort"], connect=False)
        persons_db = mongo.persons_db
        activity_person_db = mongo.activity_person_db
        self.activity_person = activity_person_db.activity_person
        self.person_face = persons_db.persons
        self.face_locations = []
        self.face_encodings = []
        self.face_names = []
        self.process_this_frame = True
        self.face_encodings_list = []
        self.count_frame = 1
        self.video_capture = cv2.VideoCapture(settings["typeCam"])
        self.redis = redis.StrictRedis(
            host=settings["redisIP"], port=settings["redisPort"], db=settings["redisDB"])

    def __save_face_person_encoding(self, face_encoding):
        _id = uuid.uuid1()
        person = {"_id": _id,
                  "face_and_name": (face_encoding.tolist(), _id),
                  "person": True, }
        self.person_face.save(person)

    def __get_known_faces(self):
        persons = self.person_face.find({"person": True})
        encodings_and_face = [person.get("face_and_name")
                              for person in persons]
        if encodings_and_face and not None in encodings_and_face:
            known_face_encodings, known_face_names = zip(*encodings_and_face)
            return known_face_encodings, known_face_names

    def __send_to_save_and_clear_face_encoding(self, face_encoding, known_face_encodings):
        self.count_frame += 1

        if not len(self.face_encodings_list) or self.count_frame % settings["step_frame_for_check"] == 0:
            self.face_encodings_list.append(face_encoding)

        modulo = math.modf(self.count_frame / settings["step_frame_for_check"])[0]
        
        if self.face_encodings_list and (0.2 < modulo < 0.8):
            matches_in_face_encodings = face_recognition.compare_faces(
                self.face_encodings_list, face_encoding, tolerance=0.2)
            matches_in_known_face_encodings = face_recognition.compare_faces(
                known_face_encodings, face_encoding, tolerance=0.62)

            if True in matches_in_face_encodings and not True in matches_in_known_face_encodings:
                self.__save_face_person_encoding(face_encoding)
                self.face_encodings_list = []
                self.count_frame = 0

        if len(self.face_encodings_list) > settings["num_frame_for_check"]:
            self.face_encodings_list = []
            self.count_frame = 0

    def __get_match_known_faces(self):
        self.face_names = []
        for face_encoding in self.face_encodings:
            # See if the face is a match for the known face(s)
            known_faces = self.__get_known_faces()
            if known_faces:
                known_face_encodings, known_face_names = known_faces
            else:
                known_face_encodings = []
                known_face_names = []
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding, tolerance=0.5)
            name = "Unknown"

            if not any(matches):
                self.__send_to_save_and_clear_face_encoding(
                    face_encoding, known_face_encodings)

            # If a match was found in known_face_encodings, just use the first one.
            if True in matches:
                first_match_index = matches.index(True)
                name = str(known_face_names[first_match_index])
                id_person = known_face_names[first_match_index]
                self.__count_activity_person(str(id_person))

            self.face_names.append(name)

    def __release_handle(self, video_capture):
        video_capture.release()
        cv2.destroyAllWindows()

    def __count_activity_person(self, id_person):
        is_activity = self.redis.get(id_person)
        if not is_activity:
            count_activity = {}
        else:
            count_activity = json.loads(is_activity.decode("utf-8"))

        count_activity["count"] = count_activity.get("count", 0) + 1
        count_activity["datetime"] = repr(datetime.datetime.now())
        self.redis.set(id_person, json.dumps(count_activity))

    def __clear_not_activity_person(self):
        keys_activity_persons = self.redis.keys()
        if keys_activity_persons:
            for key in keys_activity_persons:
                activity_person = json.loads(
                    self.redis.get(key).decode("utf-8"))
                last_activity = activity_person.get("datetime")
                time_now = datetime.datetime.now()
                delta = (time_now - eval(last_activity)).total_seconds()
                if delta > settings["delta_time"]:
                    self.redis.delete(key)

    def __display_results(self, frame):
        for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35),
                          (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6),
                        font, 1.0, (255, 255, 255), 1)
        cv2.imshow('Video', frame)

    def __read_frame(self):
        """ Frame generator. """
        print('# Hit "q" to quit!')
        while True:
            __, image = self.video_capture.read()
            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
                break
            else:
                yield image
        self.video_capture.release()
        return

    def __detect_faces(self, image):
        shrink = settings["image_scale_factor"]
        light_image = cv2.resize(image, (0, 0), fx=1 / shrink, fy=1 / shrink)
        rgb_light_image = light_image[:, :, ::-1]
        light_image_face_locations = face_recognition.face_locations(
            rgb_light_image)
        self.face_encodings = face_recognition.face_encodings(
            rgb_light_image, light_image_face_locations)
        self.face_locations = list(tuple(spot * shrink for spot in box)
                                   for box in light_image_face_locations)

    def search_and_save_person(self):
        [self.redis.delete(key) for key in self.redis.scan_iter()] # cleare redis
        for frame in self.__read_frame():
            if self.process_this_frame:  # only process every second frame of video to save time
                self.__detect_faces(frame)
                self.__get_match_known_faces()
                self.__display_results(frame)
                self.__clear_not_activity_person()
            self.process_this_frame = not self.process_this_frame
        cv2.destroyAllWindows()


if __name__ == "__main__":
    recognition = PersonRecognition()
    recognition.search_and_save_person()
