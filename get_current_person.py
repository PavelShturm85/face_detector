import redis
import json

settings = dict(
    redisIP="localhost",
    redisPort=6379,
    redisDB=0,
)

class CurrentCustomer():
    def __init__(self, *args, **kwargs):
        self.redis = redis.StrictRedis(host=settings["redisIP"], port=settings["redisPort"], db=settings["redisDB"])
    
    def get_current_customer(self):
        keys_activity_persons = self.redis.keys()
        if keys_activity_persons:
            counts_activity = {}
            for key in keys_activity_persons:
                activity_person = json.loads(self.redis.get(key).decode("utf-8"))
                count_activity = activity_person.get("count")
                counts_activity[key.decode("utf-8")] = int(count_activity)
            if counts_activity:
                return max(counts_activity, key=counts_activity.get)
            

if __name__ == "__main__":
    current_customer = CurrentCustomer()
    print(current_customer.get_current_customer())
