import os
commands_for_install = (
    "sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4",
    'echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list',
    "sudo apt-get update",
    "sudo apt-get install -y mongodb-org",
    "sudo service mongod start",
    "sudo apt-get install -y redis-server",
    "sudo service redis start",
    "sudo apt-get install -y build-essential",
    "sudo apt-get install -y gfortran git wget curl graphicsmagick libgraphicsmagick1-dev libatlas-dev",
    "sudo apt-get install -y libavcodec-dev libavformat-dev libgtk2.0-dev libjpeg-dev liblapack-dev",
    "sudo apt-get install -y libswscale-dev pkg-config python3-dev python3-numpy software-properties-common zip",
    "sudo apt-get install -y libgtk-3-dev",
    "sudo apt-get install -y libboost-all-dev",
    "sudo apt-get install -y libcurl4",
    "sudo apt-get install -y cmake",
    "sudo apt-get install -y virtualenv",
    "virtualenv -p python3 .env",
    ".env/bin/pip install -r requirements.txt",
    ".env/bin/python3 face_search.py",
)

for command in commands_for_install:
    try:
        os.system(command)
    except:
        pass